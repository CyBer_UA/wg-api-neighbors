var request = require( "request" ),
    async = require( "async" ),
    http = require( "http" ),
    url = require( "../lib/url" );

module.exports = function ( auth, callback ) {

    var reqUrl = url.createApiUrl( "ratings/neighbors/", {
        type: "28",
        "account_id" : auth.id,
        limit: 10,
        fields: "account_id,battles_to_play",
        "rank_field": "frags_count"
    });

    async.waterfall( [
        function ( callback ) {
            request.post ( reqUrl, callback );
        },

        function ( httpResponse, body, callback ) {
            try {
                var res = JSON.parse( body );

                if ( res.status == "error" ) {
                    return callback ( res.error );
                }

                callback ( null, res.data );
            } catch ( err )  {

                callback ( err );
            }
        }


    ], callback )

};
