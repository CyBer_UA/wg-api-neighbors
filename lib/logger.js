
var winston = require('winston'),
    path = require( "path"),
    pathToLog = path.join( __dirname, "../log/error.log" );


winston.add( winston.transports.File, { filename: './log/error.log' });

exports.logger = winston;