

var cache = require('memory-cache'),
    url = require( "./url"),
    request = require ( "request"),
    logger = require( "../lib/logger" ).logger,
    CronJob = require('cron').CronJob;





exports.updateData = function ( callback  ) {
    updateData ( callback );
};



exports.startDataVerification = function ( callback ) {

    var job = new CronJob('00 00 13 * * *',
        function() {
            updateData ( function ( err ) {
                if( err ) {
                    logger.error( err );
                }
            } );
        }
    );

    exports._job = job;
    job.start();
};

exports.stopDataVerification = function () {
    this._job.stop();
}



function updateData ( callback ) {

    tryGet( getGameVersion, function ( err, version ) {

        if ( err ) {
            return callback ( err );
        }

        if ( !cache.get( "vehicles" ) ||
            !cache.get( "version" ) ||
            version != cache.get( "version" )  ) {

            tryGet( downloadVehicles, function ( err, vehicles ) {

                if ( err ) {
                    return callback ( err );
                }

                cache.put( "vehicles", vehicles );
                cache.put( "version", version );

                callback ( null, vehicles, version );
            } );

            return;
        }

        callback ( null, cache.get( "vehicles" ), version );
    } );
};

function downloadVehicles ( callback ) {

    var reqUrl = url.createApiUrl ( "encyclopedia/tanks/", {
        fields: "name_i18n,tank_id"
    });

    request.post( reqUrl ,
        function ( err, httpResponse, body ) {

            if ( err ) {
                return callback ( err );
            }

            try {

                var response = JSON.parse( body );

                callback( null, response, response.data );

            } catch ( err ) {

                callback ( err );
            }

        } );

};


function getGameVersion ( callback ) {

    request.post( url.createApiUrl ( "encyclopedia/info/" ),
        function ( err, httpResponse, body ) {

            if ( err )
                return callback ( err );

            var response = JSON.parse( body ) ;

            try {
                callback( null, response , response.data ["game_version"] );
            } catch ( err ) {
                callback ( err );
            }
        } );

};


function tryGet ( callFnc, callbackFnc, callCount ) {

    var attempts = 0;

    if ( !callCount ) {
        callCount = 3;
    }

    (function load ( ) {

        callFnc ( function ( err, response, data ) {

            if( err &&
                response.status !== "ok" &&
                attempts != callCount ) {

                attempts++;
                load();
            };

            if ( !err &&
                response.status != "error" ) {
                err = response.error;
            }

            callbackFnc ( err, data );

        } );


    } () );

};