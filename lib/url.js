
var url = require ( "url"),
    conf = require( "../config").conf ;

module.exports = {
    parse: function ( url ) {

        var urlObj = {};

        url.split("&").forEach( function ( val ) {

            var keyVal = val.split( "=" ) ;

            if ( keyVal.length != 2 ) return;

            urlObj [ keyVal [0] ] = keyVal [1];


        } );

        return urlObj;

    },

    createApiUrl: function ( method, params ) {

        if ( !isObject( params ) ) {
            params = {};
        }

        var urlApi = "https://api.worldoftanks.ru/wot/";

        urlApi =  url.resolve( urlApi , method ) + "?";

        params [ "application_id" ] = conf.get( "app_id" );


        Object.keys( params ).forEach( function ( key ) {
            urlApi += key +"="+ params [ key ] + "&";
        } );

        return urlApi;
    }
};


function isObject( obj ) {
    return {}.toString.call( obj ) === '[object Object]';
};
