var request = require ( "request" ),
    url = require( "../lib/url");



module.exports = function ( neighbords, callback ) {

    var tanksCollection = {},
        errLog = [],
        calledLeng = neighbords.length,
        calledCount = 0,
        id,
        paramsTanks = {
            fields: "all,tank_id,account_id"
        },
        paramsNames = {
            fields: "nickname"
        },
        createApiUrlTanks = url.createApiUrl.bind( null, "tanks/stats/" ),
        createApiUrlNames = url.createApiUrl.bind( null, "account/info/" );



    neighbords.forEach ( function ( user, i ) {

        id = user [ "account_id" ];

        paramsTanks [ "account_id" ] = id;

        request.post ( createApiUrlTanks( paramsTanks ), tanksCallback.bind( null, id, i ) );


    } );


    function tanksCallback ( id, i, err, httRes, body ) {

        if ( err ) {
            return setErr( err );
        }

        var tanks = parseData ( body );

        if ( !tanks || !tanks.data )
            return;

        tanksCollection [ id ] = {
            index: i,
            tanksStats: tanks.data [ id ]
        };

        paramsNames [ "account_id" ] = id;

        request.post( createApiUrlNames ( paramsNames ),
            namesCallback.bind ( null, id ) );


    };

    function namesCallback ( id, err, httRes, body ) {

        var name;

        checkCallback ();

        if ( err ) {
            return setErr ( err );
        }

        if ( !( name = parseData( body ) ) )
            return;

        name = name.data [ id ] [ "nickname" ];
        tanksCollection [ id ].nickname = name;
    };


    function parseData ( data ) {

        try {
            data = JSON.parse( data );


            if ( data.response === "error" ) {
               return setErr( data.error );
            }


            return data;

        } catch ( err ) {
            setErr( err );
        }

    };


    function checkCallback () {

        var error ;
        if ( ++calledCount === calledLeng ) {

            if ( !errLog.length )
                error = null;

            callback ( error, tanksCollection );
        }
    };

    function setErr ( err  ) {
        errLog.push( err );
        calledLeng --;
    }


};