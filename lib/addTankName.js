var cache = require('memory-cache');


module.exports = function ( neighbors, callback ) {

    var vehicles = cache.get ( "vehicles" );

    Object.keys ( neighbors ).forEach( function ( id ) {

      neighbors [ id ].tanksStats.forEach ( function ( stats ) {

          stats [ "tank_name" ] = vehicles[ stats [ "tank_id" ] ] [ "name_i18n" ] ;

      } );


    }  );

    callback( null, neighbors );

};