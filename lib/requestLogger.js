var fs = require( "fs-extra" ),
    morgan = require('morgan'),
    path = require( "path" );


module.exports = function ( app ) {

    var pathToLog = path.join ( __dirname, "../log" );

    fs.mkdirpSync ( pathToLog );

    var accessLogStream = fs.createWriteStream( path.join( pathToLog, "request.log" ) , {flags: 'a'}),
        logger = morgan('combined', {stream: accessLogStream});

    app.use( logger );
};