var conf = require ( "../config").conf,
    urlLib = require( "../lib/url" ),
    url = require ( "url" );

module.exports = function ( req, res, next ) {

   var auth = req.signedCookies.auth,
       pathname = url.parse( req.url).pathname.trim(),
       redirectUrl = url.format( {
        protocol: conf.get( "protocol" ),
        port: conf.get( "port" ),
        hostname: conf.get( "hostname" ),
        pathname: "/login"
       });

    if( !auth &&
        pathname != "/login" ) {

        res.redirect ( urlLib.createApiUrl( "auth/login/", {
            "redirect_uri": redirectUrl
        } ) );
        return;
    };

    next ();
};