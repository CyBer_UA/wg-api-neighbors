(function ( $ ) {

    var playersList,
        isReady,
        playerId,
        isDataDownload;

    $.post( "/playerslist", function  ( res ) {

        if ( res.status == "error" ) {
            alert( "Тут типо обработка ошибок " + res.error );
            return;
        };


        playersList = res.data;
        isDataDownload = true;
        playerId = playersList [ "current_player_id" ];
        delete playersList [ "current_player_id" ];

        renderTable ();
    } );


    $( function () {
        isReady = true;
        renderTable ();
    } );

    function renderTable () {

        if ( !isDataDownload ||
            !isReady ) return;

        var tHeaderFragment = document.createDocumentFragment(),
            tBodyFragment = document.createDocumentFragment(),
            winRate,
            player,
            headerTr = document.createElement( "tr"),
            tr,
            td,
            tankId,
            playersIds = Object.keys ( playersList ),
            emptyLine = createEmptyLine(),
            renderedVehicles = {};

        headerTr.innerHTML = emptyLine;
        headerTr.children [0].textContent = "Tank Name"
        tHeaderFragment.appendChild( headerTr );

        playersIds.sort( function ( a, b) {
            a = playersList [ a ].index;
            b = playersList [ b ].index;

            if (a > b) return 1;
            if (a < b) return -1;
        } );

        playersIds.forEach( function ( id, i ) {

           player = playersList [ id ];
           td = headerTr.children [ i + 1 ];


           if ( id == playerId ) {
               td.className = "current_player";
           }

           td.textContent = player.nickname;

           player.tanksStats.forEach ( function ( stats ) {

              winRate = Math.round ( (stats.all.wins / stats.all.battles ) * 100  );
              tankId = stats ["tank_id"];

              if( !renderedVehicles [ tankId ] ) {
                  tr = document.createElement( "tr" );
                  tr.innerHTML = emptyLine;
                  tr.children [ 0 ].textContent = stats [ "tank_name" ];
                  tr.children [ 0 ].className = "tank_name";
                  tBodyFragment.appendChild( tr );
                  renderedVehicles [ tankId ] = tr;
              }
             if( !stats.all.battles  ) {
                 renderedVehicles [ tankId ].children [ i + 1 ].textContent = 'Нет боев, в "случайных боях"';
                 return;
             }

             renderedVehicles [ tankId ].children [ i + 1 ].textContent = winRate + "%";


           } );


        } );

        $("#users_stats_head").append ( tHeaderFragment );
        $("#users_stats_body").append ( tBodyFragment );
        $("#users_stats_table").show();
        $("#loader-wrapper").hide();


        function createEmptyLine ( ) {

            var length = playersIds.length + 2;
            return new Array( length ).join( "<td></td>" );

        }
    };






} ( jQuery ));