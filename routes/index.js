var logger = require( "../lib/logger" ).logger;

module.exports = function( app ){

    app.get ( "/", require ( "./main.js" ) );
    app.get ( "/login", require ( "./login" ) );
    app.post ( "/playerslist", require( "./playerslist" ));

    app.get ( "/logout", function ( req, res ) {
       res.cookie ( "auth" );
       res.redirect ( "/" );
    } );

    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });


    app.use(function(err, req, res, next) {

        logger.error ( err );

        res.render('error', {
            message: err.message,
            error: err
        });
    });

};
