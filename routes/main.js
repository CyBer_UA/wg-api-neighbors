var url = require ( "../lib/url");

module.exports = function ( req, res ) {

    var auth = req.signedCookies.auth;

    res.render ( "index", {
        nickname: auth.name,
        "user_id": auth.id
    } );
};

