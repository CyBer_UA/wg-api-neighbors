var url = require ( "../lib/url" );

module.exports = function ( req, res ) {

    if( req.signedCookies.auth ) {
    
       return res.redirect ( "/" );
    
    }

    var urlObj = url.parse ( req.url );
    var value = {
        token: urlObj [ "access_token"],
        name: urlObj [ "nickname" ],
        id: urlObj [ "account_id" ],//28094,//
        expires: urlObj [ "expires_at" ]
    };

    res.cookie ( "auth", value, {
        signed: true,
        httOnly: true,
        path: "/",
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000
    } );

    res.redirect ( "/" );

};



