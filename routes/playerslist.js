var async = require( "async"),
    logger = require( "../lib/logger" ).logger;

module.exports = function ( req, res ) {

    var neighbors = require ( "../lib/getNeighbors"),
        auth = req.signedCookies.auth;

    async.waterfall( [
        async.apply( neighbors, auth ),
        require ( "../lib/getNeighborsTanks" ),
        require ( "../lib/addTankName" )
    ],function( err, players ) {
        if ( err ) {

            logger.error ( err );
            res.json ( {
                status: "error",
                error: error
            } );
        }

        players [ "current_player_id" ] = auth.id;


        res.json ( {
            status: "ok",
            data: players
        } );
    } );


};
