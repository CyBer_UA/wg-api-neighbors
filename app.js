var express = require('express'),
    http = require('http'),
    path = require('path'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    conf = require( "./config/index.js" ).conf,
    cache = require( "./lib/cache" ),
    logger = require( "./lib/logger" ).logger,
    url = require( "url" );

var app = express();

cache.startDataVerification ();
cache.updateData( function ( err ) {
    if( err ) {
        logger.error( err );
    }

} );


require( "./lib/requestLogger" ) ( app ) ;
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'template'));
app.set( "env", "development" );



//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser( "secret" ));



app.use(express.static(path.join(__dirname, 'public')));


app.use ( require( "./middleware/authorization.js" ) );
require ( "./routes/index.js" ) ( app );

var port = process.env.PORT || conf.get( "port" );

app.listen( port, function () {
    conf.set( "port", port );
    var startedUrl = url.format ( {
        port: port
    } );

    console.log( "server started on " +  startedUrl);
});





module.exports = app;
